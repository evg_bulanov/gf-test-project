package com.gofluent.answer.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfiguration {
    // application configuration here
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
