package com.gofluent.answer.conf;

import com.gofluent.answer.dao.*;
import com.gofluent.answer.dao.model.ExamEntity;
import com.gofluent.answer.dao.model.ExamExecutionEntity;
import com.gofluent.answer.dao.model.ExamQuestionEntity;
import com.gofluent.answer.dao.model.PersonEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component @RequiredArgsConstructor
public class ApplicationStubDataInitializer {
    private final ExamAnswerRepository examAnswerRepository;
    private final PersonRepository personRepository;
    private final ExamRepository examRepository;
    private final ExamExecutionRepository examExecutionRepository;
    private final ExamQuestionRepository examQuestionRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void initializeStubData() {
        if (!personRepository.findAll().isEmpty()) {
            System.out.println("Database is not empty, skip stub data initialization ...");
            return;
        }

        // init a couple of persons
        PersonEntity jan = new PersonEntity()
                .setId("7c03881f-64a7-40df-9a68-f4bf89f04015")
                .setName("Jan Jansen")
                .setEmail("jan-jansen@gmail.com");

        PersonEntity ivan = new PersonEntity()
                .setId("a0ff3571-eaa3-4368-83e0-6c79605b6e8e")
                .setName("Ivan Ivanov")
                .setEmail("ivan-ivanov@gmail.com");

        personRepository.save(jan);
        personRepository.save(ivan);

        // init several exams
        ExamEntity ielts = new ExamEntity()
                .setId("487179cc-2e86-4662-82cd-62ca7a1170c3")
                .setName("IELTS (International English Language Testing System)")
                .setLanguage("English")
                .setLevel("B2");

        ExamEntity toefl = new ExamEntity()
                .setId("19e8741d-518f-42bf-89ed-240adaaf41be")
                .setName("TOEFL (Test of English as a Foreign Language)")
                .setLanguage("English")
                .setLevel("C1");

        ExamEntity cee = new ExamEntity()
                .setId("6bab8152-e335-4c64-bf23-1436dfa78c7d")
                .setName("Cambridge English Exams")
                .setLanguage("English")
                .setLevel("CPE");
        examRepository.save(ielts);
        examRepository.save(toefl);
        examRepository.save(cee);

        // init questions
        ExamQuestionEntity question1 = new ExamQuestionEntity()
                .setId("45a62e10-2951-44d4-b2f1-5d6ea3c06333")
                .setExamId(ielts.getId())
                .setQuestion("Some people believe that technology has made our lives more complicated, while others disagree. " +
                        "Discuss both views and give your opinion.");

        ExamQuestionEntity question2 = new ExamQuestionEntity()
                .setId("caeb9d2c-2144-4895-ae52-211306e98330")
                .setExamId(toefl.getId())
                .setQuestion("In many countries, the consumption of fast food is increasing. " +
                        "Discuss the advantages and disadvantages of this trend.");

        ExamQuestionEntity question3 = new ExamQuestionEntity()
                .setId("5c9748ef-52f7-492e-81e0-f025abbfc92e")
                .setExamId(cee.getId())
                .setQuestion("Some people argue that the best way to improve road safety is to increase the legal driving age. " +
                        "To what extent do you agree or disagree?");

        examQuestionRepository.save(question1);
        examQuestionRepository.save(question2);
        examQuestionRepository.save(question3);

        // start test executions
        ExamExecutionEntity janIeltsExecution = new ExamExecutionEntity()
                .setId("30f6efc4-c2aa-41ef-abec-6da9d1bd679a")
                .setPersonId(jan.getId())
                .setExamId(ielts.getId())
                .setStartTime(LocalDateTime.now().minusDays(2));

        ExamExecutionEntity janToeflExecution  = new ExamExecutionEntity()
                .setId("5904bc36-87e3-41ce-92af-52b1a1102a0b")
                .setPersonId(jan.getId())
                .setExamId(toefl.getId())
                .setStartTime(LocalDateTime.now().minusWeeks(1));

        ExamExecutionEntity ivanCeeExecution  = new ExamExecutionEntity()
                .setId("9b93bcaf-da3a-4dbb-8421-9c232aa74471")
                .setPersonId(ivan.getId())
                .setExamId(cee.getId())
                .setStartTime(LocalDateTime.now().minusWeeks(1));

        examExecutionRepository.save(janIeltsExecution);
        examExecutionRepository.save(janToeflExecution);
        examExecutionRepository.save(ivanCeeExecution);
    }
}
