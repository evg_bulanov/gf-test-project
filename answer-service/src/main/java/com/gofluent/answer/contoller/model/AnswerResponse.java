package com.gofluent.answer.contoller.model;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class AnswerResponse {
    private String id;
    private String executionId;
    private String questionId;
    private String answer;
    private Boolean answerCorrect;
    private String correctedAnswer;
}
