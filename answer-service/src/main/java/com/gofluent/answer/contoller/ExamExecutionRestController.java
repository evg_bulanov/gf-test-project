package com.gofluent.answer.contoller;

import com.gofluent.answer.contoller.model.ExamExecutionResponse;
import com.gofluent.answer.dao.ExamExecutionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ExamExecutionRestController {
    private final ExamExecutionRepository repository;

    @GetMapping(path = "execution")
    public List<ExamExecutionResponse> getExecutions() {
        return repository.findAll().stream()
                .map(e -> new ExamExecutionResponse(e.getId(), e.getPersonId(), e.getExamId(), e.getStartTime()))
                .toList();
    }
}
