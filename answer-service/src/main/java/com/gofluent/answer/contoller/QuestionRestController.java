package com.gofluent.answer.contoller;

import com.gofluent.answer.contoller.model.QuestionResponse;
import com.gofluent.answer.dao.ExamQuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class QuestionRestController {
    private final ExamQuestionRepository repository;

    @GetMapping(path = "question")
    public List<QuestionResponse> getQuestions() {
        return repository.findAll().stream()
                .map(e -> new QuestionResponse(e.getId(), e.getExamId(), e.getQuestion()))
                .toList();
    }
}
