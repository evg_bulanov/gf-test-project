package com.gofluent.answer.contoller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class QuestionResponse {
    private String id;
    private String examId;
    private String question;
}
