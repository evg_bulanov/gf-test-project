package com.gofluent.answer.contoller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter @Setter @Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
public class ExamExecutionResponse {
    private String id;
    private String personId;
    private String examId;
    private LocalDateTime startTime;
}
