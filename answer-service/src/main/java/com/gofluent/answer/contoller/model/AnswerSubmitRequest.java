package com.gofluent.answer.contoller.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class AnswerSubmitRequest {
    private String executionId;
    private String questionId;
    private String answer;
}
