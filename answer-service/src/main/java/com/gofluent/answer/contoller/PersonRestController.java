package com.gofluent.answer.contoller;


import com.gofluent.answer.contoller.model.PersonResponse;
import com.gofluent.answer.dao.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PersonRestController {
    private final PersonRepository repository;

    @GetMapping(path = "person")
    public List<PersonResponse> getPersons() {
        return repository.findAll().stream()
                .map(e -> new PersonResponse(e.getId(), e.getName(), e.getEmail()))
                .toList();
    }
}
