package com.gofluent.answer.contoller;


import com.gofluent.answer.contoller.model.ExamResponse;
import com.gofluent.answer.dao.ExamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ExamRestController {
    private final ExamRepository repository;

    @GetMapping(path = "exam")
    public List<ExamResponse> getExams() {
        return repository.findAll().stream()
                .map(e -> new ExamResponse(e.getId(), e.getName(), e.getLanguage(), e.getLevel()))
                .toList();
    }
}
