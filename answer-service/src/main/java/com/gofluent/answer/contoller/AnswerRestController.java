package com.gofluent.answer.contoller;

import com.gofluent.answer.contoller.model.AnswerResponse;
import com.gofluent.answer.contoller.model.AnswerSubmitRequest;
import com.gofluent.answer.contoller.model.AnswerSubmitResponse;
import com.gofluent.answer.dao.ExamAnswerRepository;
import com.gofluent.answer.service.AnswerSubmitService;
import com.gofluent.answer.service.model.AnswerSubmitResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AnswerRestController {
    private final ExamAnswerRepository repository;
    private final AnswerSubmitService answerSubmitService;

    @GetMapping(path = "answer")
    public List<AnswerResponse> getAnswers() {
        return repository.findAll().stream()
                .map(e -> new AnswerResponse(e.getId(), e.getExecutionId(), e.getQuestionId(), e.getAnswer(),
                    e.getAnswerCorrect(), e.getCorrectedAnswer()))
                .toList();
    }

    @PostMapping(path = "answer/submit")
    public AnswerSubmitResponse submitAnswer(@RequestBody AnswerSubmitRequest request) {
        AnswerSubmitResult result = answerSubmitService.submitAnswer(request.getExecutionId(), request.getQuestionId(), request.getAnswer());
        return new AnswerSubmitResponse()
                .setId(result.getAnswerEntity().getId())
                .setAnswerIsCorrect(result.isAnswerCorrect())
                .setAnswerCorrection(result.getCorrectedAnswer());
    }
}
