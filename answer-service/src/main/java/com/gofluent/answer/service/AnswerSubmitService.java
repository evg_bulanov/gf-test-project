package com.gofluent.answer.service;

import com.gofluent.answer.dao.ExamAnswerRepository;
import com.gofluent.answer.dao.model.ExamAnswerEntity;
import com.gofluent.answer.service.model.AnswerSubmitResult;
import com.gofluent.answer.service.model.CheckAnswerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AnswerSubmitService {
    private final ExamAnswerRepository repository;
    private final AnswerCheckService answerCheckService;

    @Transactional
    public AnswerSubmitResult submitAnswer(String executionId, String questionId, String answer) {
        CheckAnswerResult checkResult = answerCheckService.checkAnswer(answer);

        ExamAnswerEntity answerEntity = createAnswer(executionId, questionId, answer, checkResult.isWritingCorrect(), checkResult.getWritingCorrection());

        return new AnswerSubmitResult()
                .setAnswerEntity(answerEntity)
                .setAnswerCorrect(checkResult.isWritingCorrect())
                .setCorrectedAnswer(checkResult.getWritingCorrection());
    }

    private ExamAnswerEntity createAnswer(String executionId, String questionId, String answer, Boolean answerCorrect, String correctedAnswer) {
        ExamAnswerEntity answerEntity = new ExamAnswerEntity()
                .setId(UUID.randomUUID().toString())
                .setQuestionId(questionId)
                .setExecutionId(executionId)
                .setAnswer(answer)
                .setAnswerCorrect(answerCorrect)
                .setCorrectedAnswer(correctedAnswer);
        repository.save(answerEntity);
        return answerEntity;
    }
}
