package com.gofluent.answer.service.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true)
public class CheckAnswerResult {
    private boolean writingCorrect;
    private String writingCorrection;
}
