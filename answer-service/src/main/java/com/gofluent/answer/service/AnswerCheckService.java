package com.gofluent.answer.service;

import com.gofluent.answer.service.model.CheckAnswerResult;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class AnswerCheckService {
    private final RestTemplate restTemplate;

    @Value("${answer.service.url}")
    private String answerServiceUrl;

    public CheckAnswerResult checkAnswer(String answerToCheck) {
        WritingCheckResponse response = restTemplate.postForObject(answerServiceUrl, answerToCheck, WritingCheckResponse.class);

        return new CheckAnswerResult()
                .setWritingCorrect(response.writingCorrect)
                .setWritingCorrection(response.writingCorrection);
    }

    @Getter @Setter
    public static class WritingCheckResponse {
        private boolean writingCorrect;
        private String writingCorrection;
    }
}
