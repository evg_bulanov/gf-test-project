package com.gofluent.answer.service.model;

import com.gofluent.answer.dao.model.ExamAnswerEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true)
public class AnswerSubmitResult {
    private ExamAnswerEntity answerEntity;
    private boolean answerCorrect;
    private String correctedAnswer;
}
