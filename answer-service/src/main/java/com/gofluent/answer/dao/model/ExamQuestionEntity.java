package com.gofluent.answer.dao.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "exam_question")
@Getter @Setter @Accessors(chain = true)
public class ExamQuestionEntity {
    @Id
    @Column(name="id", nullable = false)
    @Access(AccessType.PROPERTY)
    private String id;
    @Column(name="exam_id", nullable = false)
    private String examId;
    @Column(name="question", nullable = false)
    private String question;
}
