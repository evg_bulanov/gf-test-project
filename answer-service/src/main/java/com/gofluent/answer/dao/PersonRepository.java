package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<PersonEntity, String> {
}
