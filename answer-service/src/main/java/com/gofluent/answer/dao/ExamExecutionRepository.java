package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamExecutionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamExecutionRepository extends JpaRepository<ExamExecutionEntity, String> {
}
