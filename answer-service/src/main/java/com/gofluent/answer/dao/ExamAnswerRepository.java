package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamAnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamAnswerRepository extends JpaRepository<ExamAnswerEntity, String> {
}
