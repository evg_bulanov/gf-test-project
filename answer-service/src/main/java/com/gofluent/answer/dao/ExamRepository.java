package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamRepository extends JpaRepository<ExamEntity, String> {
}
