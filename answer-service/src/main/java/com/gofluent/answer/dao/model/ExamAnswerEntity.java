package com.gofluent.answer.dao.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "exam_answer")
@Getter @Setter @Accessors(chain = true)
public class ExamAnswerEntity {
    @Id
    @Column(name="id", nullable = false)
    @Access(AccessType.PROPERTY)
    private String id;
    @Column(name="exam_execution_id", nullable = false)
    private String executionId;
    @Column(name="exam_question_id", nullable = false)
    private String questionId;
    @Column(name="answer", nullable = false)
    private String answer;
    @Column(name="answer_correct")
    private Boolean answerCorrect;
    @Column(name="corrected_answer")
    private String correctedAnswer;
}
