package com.gofluent.answer.dao.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "person")
@Getter @Setter @Accessors(chain = true)
public class PersonEntity {
    @Id
    @Column(name="id", nullable = false)
    @Access(AccessType.PROPERTY)
    private String id;
    @Column(name="name", nullable = false)
    private String name;
    @Column(name="email", nullable = false)
    private String email;
}
