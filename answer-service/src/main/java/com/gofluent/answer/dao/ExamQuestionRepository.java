package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamQuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamQuestionRepository extends JpaRepository<ExamQuestionEntity, String> {
}
