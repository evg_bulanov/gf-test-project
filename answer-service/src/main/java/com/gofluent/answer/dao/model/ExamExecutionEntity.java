package com.gofluent.answer.dao.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Entity
@Table (name = "exam_execution")
@Getter @Setter @Accessors(chain = true)
public class ExamExecutionEntity {
    @Id
    @Column(name="id", nullable = false)
    @Access(AccessType.PROPERTY)
    private String id;
    @Column(name="person_id", nullable = false)
    private String personId;
    @Column(name="exam_id", nullable = false)
    private String examId;
    @Column(name="start_time", nullable = false)
    private LocalDateTime startTime;
}
