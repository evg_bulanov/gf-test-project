package com.gofluent.answer.dao.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name = "exam")
@Getter @Setter @Accessors(chain = true)
public class ExamEntity {
    @Id
    @Column(name="id", nullable = false)
    @Access(AccessType.PROPERTY)
    private String id;
    @Column(name="name", nullable = false)
    private String name;
    @Column(name="language", nullable = false)
    private String language;
    @Column(name="level", nullable = false)
    private String level;
}
