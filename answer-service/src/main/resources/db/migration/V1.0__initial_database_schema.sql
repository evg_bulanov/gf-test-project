create table person (
    id varchar(255) not null,
    name varchar(255) not null,
    email varchar(255) not null,
    primary key (id)
);

create table exam (
    id varchar(255) not null,
    name varchar(255) not null,
    language varchar(255) not null,
    level varchar(255) not null,
    primary key (id)
);

create table exam_question (
    id varchar(255) not null,
    exam_id varchar(255) not null
        constraint exam_id_constraint
            references exam(id),
    question text not null,
    primary key (id)
);

create index idx_exam_question_exam_id
    on exam_question (exam_id);


create table exam_execution (
    id varchar(255) not null,
    person_id varchar(255) not null
        constraint person_id_constraint
            references person(id),
    exam_id varchar(255) not null
        constraint exam_id_constraint
            references exam(id),
    start_time timestamp not null DEFAULT CURRENT_TIMESTAMP,
    primary key (id)
);

create index idx_exam_execution_person_id
    on exam_execution (person_id);

create index idx_exam_execution_exam_id
    on exam_execution (exam_id);

create table exam_answer (
    id varchar(255) not null,
    exam_execution_id varchar(255) not null
        constraint exam_execution_id_constraint
            references exam_execution(id),
    exam_question_id varchar(255) not null
        constraint exam_question_id_constraint
            references exam_question(id),
    answer text not null,
    primary key (id)
);

create index idx_exam_answer_exam_execution_id
    on exam_answer (exam_execution_id);

create index idx_exam_answer_exam_exam_question_id
    on exam_answer (exam_question_id);
