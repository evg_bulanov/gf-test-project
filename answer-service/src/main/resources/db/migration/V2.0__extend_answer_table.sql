alter table exam_answer
    add column answer_correct bool;

alter table exam_answer
    add column corrected_answer text;
