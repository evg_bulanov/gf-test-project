package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.PersonEntity;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class PersonRepositoryTest {
    @Autowired
    PersonRepository repository;

    @Test
    @Transactional
    void findById() {
        // given:
        String uuid = UUID.randomUUID().toString();
        PersonEntity person = new PersonEntity()
                .setId(uuid).setName("Test Testov")
                .setEmail("test@test.com");

        repository.save(person);

        // when:
        Optional<PersonEntity> actual = repository.findById(uuid);

        // then:
        assertTrue(actual.isPresent());
    }
}