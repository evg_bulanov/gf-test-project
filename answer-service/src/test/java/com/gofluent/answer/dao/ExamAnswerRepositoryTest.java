package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.*;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class ExamAnswerRepositoryTest {
    @Autowired
    ExamAnswerRepository examAnswerRepository;
    @Autowired
    PersonRepository personRepository;
    @Autowired
    ExamRepository examRepository;
    @Autowired
    ExamExecutionRepository examExecutionRepository;
    @Autowired
    ExamQuestionRepository examQuestionRepository;

    @Test
    @Transactional
    void findById() {
        // given:
        String personUuid = UUID.randomUUID().toString();
        PersonEntity person = new PersonEntity()
                .setId(personUuid)
                .setName("Test Testov")
                .setEmail("test@test.com");
        personRepository.save(person);

        String examUuid = UUID.randomUUID().toString();
        ExamEntity exam = new ExamEntity()
                .setId(examUuid)
                .setName("TOEFL English test B2")
                .setLanguage("English")
                .setLevel("B2");
        examRepository.save(exam);

        String examQuestionUuid = UUID.randomUUID().toString();
        ExamQuestionEntity question = new ExamQuestionEntity()
                .setId(examQuestionUuid)
                .setExamId(exam.getId())
                .setQuestion("Can you give us your tips for a work-life balance?");
        examQuestionRepository.save(question);

        String examExecutionUuid = UUID.randomUUID().toString();
        ExamExecutionEntity examExecution = new ExamExecutionEntity()
                .setId(examExecutionUuid)
                .setExamId(exam.getId())
                .setPersonId(person.getId())
                .setStartTime(LocalDateTime.now());
        examExecutionRepository.save(examExecution);

        String examAnswerUuid = UUID.randomUUID().toString();
        ExamAnswerEntity answer = new ExamAnswerEntity()
                .setId(examAnswerUuid)
                .setQuestionId(question.getId())
                .setExecutionId(examExecution.getId())
                .setAnswer("Recently, our company encourage us to take a vocation during summer holiday. That's a wonderful plan, " +
                        "which give employees more time to get around or stay with family. For me, after discussing with some colleagues, " +
                        "whom I have good relationship with, we decided to take a trip instead of long travel consider to epidemic situation. " +
                        "We were planning to go out this Friday. Hope we have a wonderful journey.");
        examAnswerRepository.save(answer);

        // when:
        Optional<ExamAnswerEntity> actual = examAnswerRepository.findById(examAnswerUuid);

        // then:
        assertTrue(actual.isPresent());
    }
}