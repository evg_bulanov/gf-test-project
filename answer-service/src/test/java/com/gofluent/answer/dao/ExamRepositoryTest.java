package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamEntity;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class ExamRepositoryTest {
    @Autowired
    ExamRepository repository;

    @Test
    @Transactional
    void findById() {
        // given:
        String uuid = UUID.randomUUID().toString();
        ExamEntity exam = new ExamEntity()
                .setId(uuid)
                .setName("TOEFL English test B2")
                .setLanguage("English")
                .setLevel("B2");

        repository.save(exam);

        // when:
        Optional<ExamEntity> actual = repository.findById(uuid);

        // then:
        assertTrue(actual.isPresent());
    }
}