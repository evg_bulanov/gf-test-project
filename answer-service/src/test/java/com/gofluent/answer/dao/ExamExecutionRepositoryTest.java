package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamEntity;
import com.gofluent.answer.dao.model.ExamExecutionEntity;
import com.gofluent.answer.dao.model.PersonEntity;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class ExamExecutionRepositoryTest {
    @Autowired
    PersonRepository personRepository;
    @Autowired
    ExamRepository examRepository;
    @Autowired
    ExamExecutionRepository examExecutionRepository;

    @Test
    @Transactional
    void findById() {
        // given:
        String personUuid = UUID.randomUUID().toString();
        PersonEntity person = new PersonEntity()
                .setId(personUuid)
                .setName("Test Testov")
                .setEmail("test@test.com");
        personRepository.save(person);

        String examUuid = UUID.randomUUID().toString();
        ExamEntity exam = new ExamEntity()
                .setId(examUuid)
                .setName("TOEFL English test B2")
                .setLanguage("English")
                .setLevel("B2");
        examRepository.save(exam);

        String examExecutionUuid = UUID.randomUUID().toString();
        ExamExecutionEntity examExecution = new ExamExecutionEntity()
                .setId(examExecutionUuid)
                .setExamId(exam.getId())
                .setPersonId(person.getId())
                .setStartTime(LocalDateTime.now());
        examExecutionRepository.save(examExecution);

        // when:
        Optional<ExamExecutionEntity> actual = examExecutionRepository.findById(examExecutionUuid);

        // then:
        assertTrue(actual.isPresent());
    }
}