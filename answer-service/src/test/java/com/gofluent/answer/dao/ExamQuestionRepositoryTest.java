package com.gofluent.answer.dao;

import com.gofluent.answer.dao.model.ExamEntity;
import com.gofluent.answer.dao.model.ExamQuestionEntity;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@AutoConfigureEmbeddedDatabase
class ExamQuestionRepositoryTest {
    @Autowired
    ExamRepository examRepository;
    @Autowired
    ExamQuestionRepository examQuestionRepository;

    @Test
    @Transactional
    void findById() {
        // given:
        String examUuid = UUID.randomUUID().toString();
        ExamEntity exam = new ExamEntity()
                .setId(examUuid)
                .setName("TOEFL English test B2")
                .setLanguage("English")
                .setLevel("B2");
        examRepository.save(exam);

        String examQuestionUuid = UUID.randomUUID().toString();
        ExamQuestionEntity question = new ExamQuestionEntity()
                .setId(examQuestionUuid)
                .setExamId(exam.getId())
                .setQuestion("What year did you _____ university?");
        examQuestionRepository.save(question);

        // when:
        Optional<ExamQuestionEntity> actual = examQuestionRepository.findById(examQuestionUuid);

        // then:
        assertTrue(actual.isPresent());
    }
}