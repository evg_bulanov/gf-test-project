package com.gofluent.openai.contoller.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true)
public class WritingCheckResponse {
    private boolean writingCorrect;
    private String writingCorrection;
}
