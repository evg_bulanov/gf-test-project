package com.gofluent.openai.contoller;

import com.gofluent.openai.contoller.model.WritingCheckResponse;
import com.gofluent.openai.service.CheckerWritingService;
import com.gofluent.openai.service.OpenAIException;
import com.gofluent.openai.service.model.writing.WritingCheckResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.apache.logging.log4j.util.Strings.isBlank;

@RestController
@Slf4j
@RequiredArgsConstructor
public class CheckWritingRestController {
    private final CheckerWritingService service;

    @PostMapping("writing/check")
    public WritingCheckResponse checkWriting(@RequestBody String writing) {
        if (isBlank(writing)) {
            throw new IllegalArgumentException("writing is empty");
        }

        WritingCheckResult checkResult = service.checkWriting(writing);

        return new WritingCheckResponse()
                .setWritingCorrect(checkResult.isCorrect())
                .setWritingCorrection(checkResult.getCorrectVariant());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public void handleIllegalArgumentException(IllegalArgumentException ex) {
        log.info(ex.getMessage());
    }

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(OpenAIException.class)
    public void handleOpenAIException(OpenAIException ex) {
        log.warn(ex.getMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public void handleException(Exception ex) {
        log.error(ex.getMessage());
    }
}
