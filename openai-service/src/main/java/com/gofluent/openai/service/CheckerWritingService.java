package com.gofluent.openai.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gofluent.openai.service.model.openai.OpenAIRequest;
import com.gofluent.openai.service.model.openai.OpenAIResponse;
import com.gofluent.openai.service.model.writing.WritingCheckResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CheckerWritingService {
    private static final String OPEN_AI_MODEL = "text-davinci-003";

    private static final String OPEN_AI_CHECK_WRITING_PROMPT =
            "Check the text below to standard English, first answer true if it's correct, false if not correct, " +
            "then provide correct variant, format result as proper JSON string with 2 columns with names: correctness, " +
                    "correct_variant: \\n\\n";

    @Autowired @Qualifier("openaiRestTemplate")
    private RestTemplate openaiRestTemplate;
    @Value("${openai.api.url}")
    private String openAiUrl;
    @Value("${openai.api.maxTokens}")
    private Integer openAiMaxTokens;


    public WritingCheckResult checkWriting(String writing) {
        String prompt = OPEN_AI_CHECK_WRITING_PROMPT + writing;
        OpenAIRequest request = new OpenAIRequest()
                .setModel(OPEN_AI_MODEL)
                .setPrompt(prompt)
                .setMax_tokens(openAiMaxTokens);

        OpenAIResponse response = openaiRestTemplate.postForObject(openAiUrl, request, OpenAIResponse.class);
        if (response == null || response.getChoices() == null || response.getChoices().isEmpty()) {
            throw new OpenAIException("Result choice not found");
        }

        try {
            String writingCheckResult =  response.getChoices().get(0).getText();
            return new ObjectMapper().readValue(writingCheckResult, WritingCheckResult.class);
        } catch (Exception e) {
            throw new OpenAIException(e.getMessage());
        }
    }
}
