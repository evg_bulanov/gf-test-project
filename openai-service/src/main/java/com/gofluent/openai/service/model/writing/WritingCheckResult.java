package com.gofluent.openai.service.model.writing;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter @Setter @Accessors(chain = true) @AllArgsConstructor @RequiredArgsConstructor
public class WritingCheckResult {
    @JsonProperty("correctness")
    private boolean correct;
    @JsonProperty("correct_variant")
    private String correctVariant;
}
