package com.gofluent.openai.service.model.openai;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Getter @Setter @Accessors(chain = true)
public class OpenAIRequest {
    private String model;
    private String prompt;
    private Integer max_tokens;
}
