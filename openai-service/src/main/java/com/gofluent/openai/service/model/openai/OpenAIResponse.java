package com.gofluent.openai.service.model.openai;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
public class OpenAIResponse {
    @Getter @Setter
    public static class OpenAIResponseChoice {
        private String text;
        private Integer index;
    }

    private String model;
    private List<OpenAIResponseChoice> choices;
}
