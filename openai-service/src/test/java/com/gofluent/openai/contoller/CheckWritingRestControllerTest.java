package com.gofluent.openai.contoller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gofluent.openai.contoller.model.WritingCheckResponse;
import com.gofluent.openai.service.CheckerWritingService;
import com.gofluent.openai.service.model.writing.WritingCheckResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@AutoConfigureMockMvc
@SpringBootTest
class CheckWritingRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CheckerWritingService service;

    @Test
    void test_notCorrectWriting() throws Exception {
        // given:
        String writingToTest = "Recently, our company encourage us to take a vocation during summer holiday.";
        String correctWriting = "Recently, our company has encouraged us to take a vacation during the summer holidays.";
        when(service.checkWriting(writingToTest)).thenReturn(
                new WritingCheckResult()
                        .setCorrect(false)
                        .setCorrectVariant(correctWriting));

        // when:
        MvcResult result = mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/writing/check")
                                .contentType(MediaType.TEXT_HTML)
                                .content(writingToTest))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // then:
        String resultJson = result.getResponse().getContentAsString();
        assertNotNull(resultJson);

        WritingCheckResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), WritingCheckResponse.class);
        assertFalse(response.isWritingCorrect());
        assertEquals(correctWriting, response.getWritingCorrection());
    }

    @Test
    void test_correctWriting() throws Exception {
        // given:
        String correctWriting = "Recently, our company has encouraged us to take a vacation during the summer holidays.";
        when(service.checkWriting(correctWriting)).thenReturn(
                new WritingCheckResult()
                        .setCorrect(true)
                        .setCorrectVariant(correctWriting));

        // when:
        MvcResult result = mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/writing/check")
                                .contentType(MediaType.TEXT_HTML)
                                .content(correctWriting))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        // then:
        String resultJson = result.getResponse().getContentAsString();
        assertNotNull(resultJson);

        WritingCheckResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), WritingCheckResponse.class);
        assertTrue(response.isWritingCorrect());
        assertEquals(correctWriting, response.getWritingCorrection());
    }
}