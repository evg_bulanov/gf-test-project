package com.gofluent.openai.service;

import com.gofluent.openai.service.model.writing.WritingCheckResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
class CheckerWritingServiceTest {
    @Autowired
    private CheckerWritingService service;
    @Autowired @Qualifier("openaiRestTemplate")
    private RestTemplate openaiRestTemplate;

    private MockRestServiceServer mockServer;

    @BeforeEach
    void setUp() {
        mockServer = MockRestServiceServer.createServer(openaiRestTemplate);
    }

    @Test
    void test_checkWriting() throws URISyntaxException {
        // given:
        String writingToTest = "She no went to the market.";

        mockServer.expect(ExpectedCount.once(),
                        requestTo(new URI("https://api.openai.com/v1/completions")))
                .andExpect(method(HttpMethod.POST))
                .andExpect(content().json("{" +
                        "  \"model\": \"text-davinci-003\"," +
                        "  \"prompt\": \"Check the text below to standard English, first answer true if it's correct, false if not correct, then provide correct variant, format result as proper JSON string with 2 columns with names: correctness, correct_variant: \\\\n\\\\nShe no went to the market.\"," +
                        "  \"max_tokens\": 500" +
                        "}"))
                .andRespond(withStatus(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{\n" +
                            "  \"id\": \"cmpl-7QnGKh1AWx8h10A51I2kVztNdsBh2\",\n" +
                            "  \"object\": \"text_completion\",\n" +
                            "  \"created\": 1686620248,\n" +
                            "  \"model\": \"text-davinci-003\",\n" +
                            "  \"choices\": [\n" +
                            "    {\n" +
                            "      \"text\": \"\\n\\n{\\\"correctness\\\": false, \\\"correct_variant\\\": \\\"She did not go to the market.\\\"}\",\n" +
                            "      \"index\": 0,\n" +
                            "      \"logprobs\": null,\n" +
                            "      \"finish_reason\": \"stop\"\n" +
                            "    }\n" +
                            "  ],\n" +
                            "  \"usage\": {\n" +
                            "    \"prompt_tokens\": 54,\n" +
                            "    \"completion_tokens\": 24,\n" +
                            "    \"total_tokens\": 78\n" +
                            "  }\n" +
                            "}\n")
                );


        // when:
        WritingCheckResult result = service.checkWriting(writingToTest);

        // then:
        assertNotNull(result);
        assertFalse(result.isCorrect());
        assertEquals("She did not go to the market.", result.getCorrectVariant());
    }
}