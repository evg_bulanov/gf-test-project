## GF Test project

### Short description (One schema can say more than 1000 words)

![](etc/wiki/main.schema.png)

### Microservices 

There are 2 microservices here: 
* answer-service
* openai-service 

### Answer service

Expose list of rest services to work with persons, exams, questions, answers, etc. 
* Persons - http://localhost:8082/person
* Exams - http://localhost:8082/exam
* Questions - http://localhost:8082/question
* Answers - http://localhost:8082/answer
* Submit answer for checking - http://localhost:8082/answer/submit

### OpenAI service

Export rest service to validate writing correction, returns correct text or not and correction.

* http://localhost:8081/writing/check

### How to build the app

```shell
./mvnw clean package -P all-services
```

it compiles services jar files and docker images

### How to start 

```shell
export OPENAI_API_KEY=sk-you-key-here
cd etc/docker/
docker-compose up -d
```
it starts containers microservices and postgres database.

### How to check 

* use http request files in etc/testing/manual
* the following curl commands

List of persons: 

```shell
curl -v http://localhost:8082/person
```

List of exams:

```shell
curl -v http://localhost:8082/exam
```

List of questions:

```shell
curl -v http://localhost:8082/question
```

List of answers:

```shell
curl -v http://localhost:8082/answer
```

### How to submit writing (answer) for checking

Run etc/testing/manual/answer.submit.request.1.http
or execute the following commaind

```shell
curl -v --header "Content-Type: application/json" \
  --request POST \
  --data '{ "executionId": "30f6efc4-c2aa-41ef-abec-6da9d1bd679a", "questionId": "45a62e10-2951-44d4-b2f1-5d6ea3c06333", "answer": "Recently, our company encourage us to take a vocation during summer holiday." }' \
  http://localhost:8082/answer/submit
```

### What is not implemented here but should be done

* Improve exception handling, timeouts, etc
* Test coverage should be extended in both services
* Security communication between microservices (ex: resource access restriction by JWT)
* CI / CD, deploy

### Note

Don't forget to specify you correct openai api key! 

